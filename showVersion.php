<?php

require_once __DIR__ . '/Maintenance.php';

class ShowVersion extends Maintenance {
	public function __construct() {
		parent::__construct();
		$this->addDescription( "Shows the current MediaWiki version and exits." );
	}

	public function execute() {
		global $wgVersion;
		$this->output( $wgVersion . "\n" );
	}
}

$maintClass = "ShowVersion";
require_once RUN_MAINTENANCE_IF_MAIN;
