# Wiki Update Reminder

Asks a local MediaWiki installation for its version.
Then checks if there is a newer version.
If there is a bugfix release, it prints to stderr.
If you are up-to-date on your minor release but there is a newer version, it prints to stdout.

## Setup

* Make sure to create a `config.json` from the `config_template.json`
* Make sure the current working directory is set to the directory the script resides in when it is called
* Wrap it with a watchdog program, which can send you annoying messages
