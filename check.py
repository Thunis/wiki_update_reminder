#!/usr/bin/env python3

import subprocess
from urllib.request import urlopen
from urllib.error import HTTPError
import json
import os.path
from sys import stderr, exit

try:
    with open("config.json","r") as f:
        config = json.loads(''.join(f.readlines()))
except FileNotFoundError as e:
    print(e, file=stderr)
    exit("You need to provide a config.json file in the current working directory. See config_template.json for an example.")

def noupdate(v):
    s = v.split(".")
    assert len(s) == 3
    return ".".join(s[:2])

def nextminor(v):
    s = v.split(".")
    s[1] = str(int(s[1])+1)
    return ".".join(s)

def nextupdate(v):
    s = v.split(".")
    s[2] = str(int(s[2])+1)
    return ".".join(s)

try:
    maintenance_dir = os.path.join(config["wikipath"],"maintenance")
except KeyError:
    exit("Your config.json needs to define the 'wikipath' value.")

try:
    with open("showVersion.php","r") as phpfile:
        verstr = subprocess.check_output(["php"], stdin=phpfile, cwd=maintenance_dir).decode('utf-8').strip()
except FileNotFoundError as e:
    print(e, file=stderr)
    if "showVersion.php" in str(e):
        exit("Could not find the php code to execute. Make sure, 'showVersion.php' is available in the current working directory.")
    elif "php" in str(e):
        exit("Please make sure, php is installed and in your PATH")
    elif maintenance_dir in str(e):
        exit("The given path doesn't seem to lead to a MediaWiki installation: %s" % config["wikipath"])
    else:
        raise e

url_base = "https://releases.wikimedia.org/mediawiki/"
url_current = url_base + noupdate(verstr)
url_next = url_base + nextminor(noupdate(verstr))

html = urlopen(url_current).read().decode('utf-8')

newupdate = nextupdate(verstr) in html
if newupdate:
    print("New security update available!", file=stderr)

try:
    urlopen(url_next)
    newminor = True
except HTTPError: # 404, because that version doesn't exist yet
    newminor = False

if newminor:
    print("New version available!")


